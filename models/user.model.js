const mongooose = require("mongoose");
const userSchema = new mongooose.Schema({
    name:{
        type:String,
        required: true,
        trim: true
    },
    phonenumber:{
        type:Number,
        required: true,
    },
    address:{
        type: String,
        required: true,
    
    },
    email:{
        type: String,
        required: true,
        unique: true,
    },
    username:{
        type: String,
        required: true,
        unique: true
    },
    password:{
            type: String,
            required: true
    },
    role:{
        type: Number,
        default: 2 //1 for admin and 2 for normal user
    }
},{
    timestamps:true
})

const userModel = mongooose.model("users",userSchema);
module.exports= userModel;