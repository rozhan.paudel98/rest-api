const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const reviewSchema = new mongoose.Schema({
    user:{
        type: Schema.Types.ObjectId,
        ref:"users"
    },
    message:{type:String, required: true},
    rating:{type:Number, required: true},
})
const productSchema = new mongoose.Schema({
    //db modelling of products
    name: {
        type: String,
        required : true,
        lowercase: true,
    },
    brand : {
        type: String,
        required: true,
    },
    description:{
        type:String,
    },
    category:{
        type: String,
        required: true,
    },
    price:{
        type: Number,
        required: true,
    },
    color:{
        type: String
    },
    image: {type:String},
    vendor:{
        type: Schema.Types.ObjectId,
        ref :"users"
    },
    reviews: [reviewSchema],
    discount:{
        discountedItem: Boolean,
        discountType: String,
        discount: String,
    },
    manuDate: Date,
    expDate: Date,
    

},{
    timestamps: true

})

const productModel= mongoose.model("products",productSchema);
const reviewModel = mongoose.model("reviews",reviewSchema);

module.exports = productModel;