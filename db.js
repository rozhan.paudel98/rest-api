const mongoose =require("mongoose");
const dbConfig = require("./configs/dbConfig");
//database connection
mongoose.connect(dbConfig.dbUrl+"/"+dbConfig.dbName,{useNewUrlParser:true,useUnifiedTopology:true},function(error,connected){
    if(error){
      console.log("Failed to connect with Message :",error);
    }
    else{
      console.log("Database Connection Success");
    }
  })