var express = require("express");

var router = express.Router();
var userModel = require("./../models/user.model");
const passwordHash = require("password-hash");

/* GET users listing. */
router.get('/', function(req, res, next) {
  //get all the users
  
  userModel.find({},{
    password: 0
  })
          .sort({
            _id : -1
          })
          .exec(function(error,users){
            if(error){
              next({
                error: error,
                msg: "No Users Found"
              });
            }
            else{
              res.json(users);
            }
          })
});

router.post("/login",function(req,res,next){
  console.log(req.body);
  userModel.findOne({username:req.body.username})
            .then(function(user){
              var isMatched = passwordHash.verify(req.body.password,user.password);
              if(isMatched){
                res.json(user);
              }
              else{
                res.json({
                  msg : "Wrong Password"
                })
              }
            })
            .catch(function(error){
              next({
                Error: error,
                msg: "NO user found"
              })
            })
})

  router.post("/register",function(request,response,next){
    
  var newUser= new userModel({});
  newUser.username = request.body.username;
  newUser.password = request.body.password;
  newUser.name= request.body.name;
  newUser.phonenumber= request.body.phonenumber;
  newUser.address= request.body.address;
  newUser.email= request.body.email;
  newUser.role =request.body.role;
  newUser.password = passwordHash.generate(request.body.password);
  newUser.save(function(error,done){
    if(error){
      console.log(error);
      next(error);
    }
    else{
      response.status(200).json(done);
    }
  })
  

  })

module.exports = router;
