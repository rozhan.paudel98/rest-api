var express = require('express');
var router = express.Router();
var productModel = require("./../models/product.model")
var mongoose = require("mongoose");
router.get('/', function(req, res, next) {
    //show all products
    var condition ={};
     productModel.find(condition)
                .sort({
                    _id: -1
                })
                .exec(function(error,product){
                    if(error){
                        next(error);
                    }
                    else{
                        res.json(product);
                    }
                })

});

router.post('/', function(req, res, next) {
    //add products
    var newProduct = new productModel({});
    if(req.body.name){
        newProduct.name= req.body.name;
    }
    if(req.body.description){
        newProduct.description= req.body.description;
    }
    if(req.body.brand){
        newProduct.brand = req.body.brand;
    }
    if(req.body.category){
        newProduct.category = req.body.category;
    }
    if( req.body.price){
        newProduct.price= req.body.price;
    }
    if( req.body.color){
        newProduct.color = req.body.color;
    }
    if( req.body.vendor){
        newProduct.vendor = req.body.vendor;
    }
    if(req.body.reviews){
            newProduct.reviews.append(req.body.reviews);
    }
    if(req.body.discount){
        newProduct.discount = req.body.discount;
    }
    newProduct.save(function(error,done){
        if(error){
            next(error);
        }
        else{
            res.json(newProduct);
        }
    })
});
router.put("/",function(req,res,next){
//update products

            

})


module.exports = router;
